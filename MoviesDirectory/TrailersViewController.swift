//
//  TrailersViewController.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 8/27/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Kingfisher

class TrailersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var no_trailers: UILabel!
    
    var in_call = false
    var trailers_url = ""
    var trailers = [Trailer]()
    
    override func viewWillAppear(_ animated: Bool) {
        no_trailers.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        safelyFetchTrailers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
    }
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trailers.count
    }
    
    // On selecting row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = trailers[indexPath.row]
        openURL(target_url: selected.video_url)
    }
    
    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrailerCell", for: indexPath) as! TrailerCell
        let current = trailers[indexPath.row]
        
        // Set title
        cell.trailer_title.text = current.title
        
        // Set image
        let url = URL(string: current.image_url)
        cell.trailer_image.kf.setImage(with: url)
        
        return cell
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    func openURL(target_url: String){
        UIApplication.shared.open(URL(string: target_url)!, options: [:], completionHandler: nil)
    }
    
    func safelyFetchTrailers(){
        if !in_call{
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyFetchTrailers()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                in_call = true
                trailers.removeAll()
                loader.startAnimating()
                fetchTrailers()
            }
        }
    }
    
    func fetchTrailers(){
        let url = URL(string: trailers_url)
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                self.endTask()
                self.showSimpleAlert(title: "Error", message: error!.localizedDescription)
            }
            else {
                do {
                    let json_response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                    
                    DispatchQueue.main.async {
                        self.endTask()
                        self.onResponse(json_response: json_response)
                    }
                }
                catch {
                    self.endTask()
                    print("Error while fetching from \(self.trailers_url)")
                }
            }
        }
        
        task.resume()
    }
    
    func onResponse(json_response: Dictionary<String, AnyObject>){
        let results = json_response["results"] as! [Dictionary<String, AnyObject>]
        for result in results{
            let type = result["type"] as! String
            if type == "Trailer"{
                let trailer = Trailer()
                let id = result["key"] as! String
                trailer.title = result["name"] as! String
                trailer.image_url = "https://img.youtube.com/vi/\(id)/hqdefault.jpg"
                trailer.video_url = "https://www.youtube.com/watch?v=\(id)"
                
                trailers.append(trailer)
            }
        }

        table_view.reloadData()
        if trailers.count == 0{
            no_trailers.alpha = 1
        }
    }
    
    func endTask(){
        loader.stopAnimating()
        in_call = false
    }

}
