//
//  ReviewsViewController.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 8/27/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class ReviewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var no_reviews: UILabel!
    
    var in_call = false
    var reviews_url = ""
    var reviews = [Review]()
    
        
    override func viewWillAppear(_ animated: Bool) {
        no_reviews.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        safelyFetchReviews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
    }
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }
    
    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
        let current = reviews[indexPath.row]
        
        cell.author.text = current.author
        cell.content.text = current.content
        return cell
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    func safelyFetchReviews(){
        if !in_call{
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyFetchReviews()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                in_call = true
                reviews.removeAll()
                loader.startAnimating()
                fetchReviews()
            }
        }
    }
    
    func fetchReviews(){
        let url = URL(string: reviews_url)
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                self.endTask()
                self.showSimpleAlert(title: "Error", message: error!.localizedDescription)
            }
            else {
                do {
                    let json_response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                    
                    DispatchQueue.main.async {
                        self.endTask()
                        self.onResponse(json_response: json_response)
                    }
                }
                catch {
                    self.endTask()
                    print("Error while fetching from \(self.reviews_url)")
                }
            }
        }
        
        task.resume()
    }
    
    func onResponse(json_response: Dictionary<String, AnyObject>){
        let results = json_response["results"] as! [Dictionary<String, AnyObject>]
        for result in results{
            let review = Review()
            review.author = result["author"] as! String
            review.content = result["content"] as! String
            reviews.append(review)
        }
        
        table_view.reloadData()
        if reviews.count == 0{
            no_reviews.alpha = 1
        }
    }
    
    func endTask(){
        loader.stopAnimating()
        in_call = false
    }
    
}
