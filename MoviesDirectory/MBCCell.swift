//
//  MBCCell.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 9/2/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class MBCCell: UITableViewCell {
    
    @IBOutlet weak var movie_title: UILabel!
    @IBOutlet weak var egypt_timing: UILabel!
    @IBOutlet weak var ksa_timing: UILabel!
    @IBOutlet weak var movie_image: UIImageView!
    @IBOutlet weak var movie_overview: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
