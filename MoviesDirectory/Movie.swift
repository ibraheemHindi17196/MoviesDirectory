//
//  Movie.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 8/27/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import Foundation

class Movie{
    var id = 0
    var title = ""
    var rating = 0.0
    var release_date = ""
    var overview = ""
    var thumbnail = ""
    var thumbnail_data = Data()
}
