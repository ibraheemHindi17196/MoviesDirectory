//
//  TrailerCell.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 8/27/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class TrailerCell: UITableViewCell {
    
    @IBOutlet weak var trailer_image: UIImageView!
    @IBOutlet weak var trailer_title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
