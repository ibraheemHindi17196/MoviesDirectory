//
//  MovieCell.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 8/27/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var release_date: UILabel!
    @IBOutlet weak var overview: UITextView!
    
    @IBOutlet weak var like_button: UIButton!
    @IBOutlet weak var share_button: UIButton!
    @IBOutlet weak var trailers_button: UIButton!
    @IBOutlet weak var reviews_button: UIButton!

    
    var container = MoviesViewController()
    var movie = Movie()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        overview.setContentOffset(CGPoint.zero, animated: false)
    }
    
}
