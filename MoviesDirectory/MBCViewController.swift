//
//  MBCViewController.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 9/2/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import SwiftSoup
import Kingfisher

class MBCViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var channel: UISegmentedControl!
    
    var mbc_movies = [MBC_Movie]()
    var in_call = false
    
    var urls = [
        "http://www.mbc.net/en/mbc2.html",
        "http://www.mbc.net/en/mbc4.html",
        "http://www.mbc.net/en/mbc-action.html"
    ]
    
    var selectors = [
        "#tab-1-0-d355d786-af8d-410d-9c82-bbcfa2627d1c",
        "#tab-1-0-a3a38426-3adc-4485-9ce9-125e4bd0bf62",
        "#tab-1-0-bbf462c8-d598-4c5c-971d-c7b3714e81a1"
    ]
    
    override func viewDidAppear(_ animated: Bool) {
        safelyFetchMBC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
    }
    
    @IBAction func onChannelChanged(_ sender: Any) {
        safelyFetchMBC()
    }
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mbc_movies.count
    }
    
    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MBCCell", for: indexPath) as! MBCCell
        let current_movie = mbc_movies[indexPath.row]

        cell.movie_title.text = current_movie.title
        cell.egypt_timing.text = current_movie.egypt_timing
        cell.ksa_timing.text = current_movie.ksa_timing
        cell.movie_overview.text = current_movie.overview
        
        let url = URL(string: current_movie.image_url)
        cell.movie_image.kf.setImage(with: url)
        
        return cell
    }
    
    func safelyFetchMBC(){
        if !in_call{
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .cancel) {UIAlertAction in
                    self.safelyFetchMBC()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                in_call = true
                mbc_movies.removeAll()
                table_view.reloadData()
                fetchMBC()
            }
        }
    }
    
    @objc func fetchMBC(){
        let index = channel.selectedSegmentIndex
        let channel_url = URL(string: urls[index])!
        let channel_selector = selectors[index]
        
        do {
            let html_string = try String(contentsOf: channel_url, encoding: .ascii)
            let doc = try SwiftSoup.parse(html_string)
            let schedule = try doc.select(channel_selector).first()!
            
            for movie in schedule.children(){
                let movie_doc = try SwiftSoup.parse(movie.html())
                let image_url = try movie_doc.select("img").first()?.attr("src")
                let title = try movie_doc.select("h3").first()?.text()
                let overview = try movie_doc.select("p").first()?.text()
                let info = try movie_doc.select("li")
                
                var egypt_timing = ""
                var ksa_timing = ""
                for item in info{
                    let current_timing = try item.text()
                    if current_timing.contains("EGYPT"){
                        egypt_timing = current_timing
                    }
                    
                    if current_timing.contains("KSA"){
                        ksa_timing = current_timing
                    }
                }
                
                let movie_obj = MBC_Movie()
                movie_obj.title = title!
                movie_obj.overview = overview!
                movie_obj.egypt_timing = egypt_timing
                movie_obj.ksa_timing = ksa_timing
                
                let final_url = "http://www.mbc.net" + image_url!
                movie_obj.image_url = final_url.replacingOccurrences(of: " ", with: "%20")
                mbc_movies.append(movie_obj)
            }

            self.table_view.reloadData()
            self.in_call = false
        }
        catch let error {
            print("Error: \(error)")
            self.in_call = false
        }
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
