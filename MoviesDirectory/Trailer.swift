//
//  Trailer.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 8/27/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

class Trailer{
    var title = ""
    var image_url = ""
    var video_url = ""
}
