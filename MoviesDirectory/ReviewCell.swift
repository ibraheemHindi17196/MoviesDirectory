//
//  ReviewCell.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 8/27/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {
    
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var content: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        content.setContentOffset(CGPoint.zero, animated: false)
    }

}
