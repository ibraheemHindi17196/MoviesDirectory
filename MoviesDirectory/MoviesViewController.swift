//
//  ViewController.swift
//  MoviesDirectory
//
//  Created by Ibraheem Hindi on 7/25/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Kingfisher
import CoreData
import SwiftSoup

class MoviesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate{
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var no_favorites: UILabel!
    @IBOutlet weak var search_bar: UISearchBar!
    
    @IBOutlet weak var favorites_button: UIButton!
    @IBOutlet weak var popular_button: UIButton!
    @IBOutlet weak var rated_button: UIButton!
    @IBOutlet weak var playing_button: UIButton!
    @IBOutlet weak var upcoming_button: UIButton!
    @IBOutlet weak var mbc_button: UIButton!
    
    
    var movies = [Movie]()
    var in_call = false
    var app_delegate = AppDelegate()
    var like_button = UIButton()
    var query = ""
    var strategy_index = 0
    var prev_index = -1
    var buttons = [UIButton]()
    
    var images_base_url = "https://image.tmdb.org/t/p/w1280"
    var urls = [
        "https://api.themoviedb.org/3/movie/popular?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US&page=1",
        
        "https://api.themoviedb.org/3/movie/top_rated?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US&page=1",
        
        "https://api.themoviedb.org/3/movie/now_playing?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US&page=1",
        
        "https://api.themoviedb.org/3/movie/upcoming?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US&page=1"
    ]

    var current_url = ""
    var trailers_url = ""
    var reviews_url = ""

    
    override func viewWillAppear(_ animated: Bool) {
        if prev_index != -1{
            strategy_index = prev_index
            updateBorders()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        no_favorites.alpha = 0
        
        if movies.count == 0{
            safelyFetchMovies(searching: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup table view
        app_delegate = UIApplication.shared.delegate as! AppDelegate
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()

        // Setup search bar
        search_bar.delegate = self
        
        buttons.append(favorites_button)
        buttons.append(popular_button)
        buttons.append(rated_button)
        buttons.append(playing_button)
        buttons.append(upcoming_button)
        buttons.append(mbc_button)
        updateBorders()
    }
 
    // ---------------- Table view ----------------
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    // Set cell height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 525
    }
    
    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = movies[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "movie_cell", for: indexPath) as! MovieCell
        
        cell.container = self
        cell.movie = movie

        // Load image
        if strategy_index == 0 && query == ""{
            cell.thumbnail.image = UIImage(data: movie.thumbnail_data)
            cell.like_button.setImage(UIImage(named: "like"), for: .normal)
        }
        else{
            let url_obj = URL(string: movie.thumbnail)
            cell.thumbnail.kf.setImage(with: url_obj)
            
            if isMovieLiked(movie_id: movie.id){
                cell.like_button.imageView?.image = UIImage(named: "like")
            }
            else{
                cell.like_button.imageView?.image = UIImage(named: "unlike")
            }
        }
        
        cell.title.text = movie.title
        cell.rating.text = String(describing: movie.rating)
        cell.release_date.text = movie.release_date
        cell.overview.text = movie.overview
        
        cell.like_button.tag = movie.id
        cell.like_button.addTarget(self, action: #selector(likeMovie(sender:)), for: .touchUpInside)
        
        cell.share_button.tag = movie.id
        cell.share_button.addTarget(self, action: #selector(shareMovie(sender:)), for: .touchUpInside)
        
        cell.trailers_button.tag = movie.id
        cell.trailers_button.addTarget(self, action: #selector(getTrailers(sender:)), for: .touchUpInside)
        
        cell.reviews_button.tag = movie.id
        cell.reviews_button.addTarget(self, action: #selector(getReviews(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    
    // ---------------- Search bar ----------------
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        query = search_bar.text!
        
        if query != ""{
            safelyFetchMovies(searching: true)
            search_bar.text = ""
            buttons[strategy_index].layer.borderWidth = 0
        }
        
        search_bar.endEditing(true)
    }
    
    func updateBorders(){
        buttons[strategy_index].layer.borderWidth = 3
        buttons[strategy_index].layer.borderColor = UIColor.white.cgColor
        
        for (array_index, item) in buttons.enumerated(){
            if array_index != strategy_index{
                item.layer.borderWidth = 0
            }
        }
    }
    
    func getMovieWithId(id: Int) -> Movie{
        for item in movies{
            if item.id == id{
                return item
            }
        }
        
        return Movie()
    }
    
    func isMovieLiked(movie_id: Int) -> Bool{
        let context = app_delegate.persistentContainer.viewContext
        let fetch_request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieEntry")
        fetch_request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(fetch_request)
            for result in results as! [NSManagedObject] {
                let id = result.value(forKey: "id") as! Int
                if id == movie_id{
                    return true
                }
            }
        }
        catch {
            print("Error")
        }
        
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.modalTransitionStyle = .crossDissolve
        
        if segue.identifier == "toTrailers"{
            let destination = segue.destination as! TrailersViewController
            destination.trailers_url = trailers_url
        }
        else if segue.identifier == "toReviews"{
            let destination = segue.destination as! ReviewsViewController
            destination.reviews_url = reviews_url
        }
    }
    
    @objc func likeMovie(sender: UIButton){
        let movie = getMovieWithId(id: sender.tag)
        like_button = sender
        safelyLikeMovie(movie: movie)
    }
    
    @objc func shareMovie(sender: UIButton){
        let movie = getMovieWithId(id: sender.tag)
        shareText(text: "Here is a movie for you. Title : \(movie.title)")
    }
    
    @objc func getTrailers(sender: UIButton){
        trailers_url = "https://api.themoviedb.org/3/movie/\(sender.tag)/videos?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US"
        
        performSegue(withIdentifier: "toTrailers", sender: self)
    }
    
    @objc func getReviews(sender: UIButton){
        reviews_url = "https://api.themoviedb.org/3/movie/\(sender.tag)/reviews?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US"
        
        performSegue(withIdentifier: "toReviews", sender: self)
    }
    
    func setStrategy(index: Int){
        strategy_index = index
        updateBorders()
        
        safelyFetchMovies(searching: false)
    }
    
    @IBAction func favorites(_ sender: Any) {
        setStrategy(index: 0)
    }
    
    @IBAction func popular(_ sender: Any) {
        setStrategy(index: 1)
    }
    
    @IBAction func topRated(_ sender: Any) {
        setStrategy(index: 2)
    }
    
    @IBAction func nowPlaying(_ sender: Any) {
        setStrategy(index: 3)
    }
    
    @IBAction func upcoming(_ sender: Any) {
        setStrategy(index: 4)
    }
    
    @IBAction func mbc(_ sender: Any) {
        prev_index = strategy_index
        setStrategy(index: 5)
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    func shareText(text: String){
        // Setup share view controller
        let text_to_share = [text]
        let share_view_controller = UIActivityViewController(
            activityItems: text_to_share,
            applicationActivities: nil
        )
        
        // Prevents crash on iPad
        share_view_controller.popoverPresentationController?.sourceView = self.view

        // present the share view controller
        self.present(share_view_controller, animated: true, completion: nil)
    }
    
    func safelyFetchMovies(searching: Bool){
        no_favorites.alpha = 0
        
        if searching{
            current_url = "https://api.themoviedb.org/3/search/movie?query=\(query)&page=1&api_key=22dffef7843a5d84428bc9f0ec48efb5"
            
            fetchMoviesHelper(searching: searching)
        }
        else if strategy_index == 5{
            performSegue(withIdentifier: "toMBC", sender: self)
        }
        else{
            if strategy_index == 0{
                loadFavorites()
            }
            else{
                current_url = urls[strategy_index - 1]
                fetchMoviesHelper(searching: searching)
            }
        }
    }
    
    func fetchMoviesHelper(searching: Bool){
        if !in_call{
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .default) {UIAlertAction in
                    self.safelyFetchMovies(searching: searching)
                })
                
                network_alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) {UIAlertAction in})
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                in_call = true
                movies.removeAll()
                loader.startAnimating()
                fetchMovies()
            }
        }
    }
    
    func fetchMovies(){
        let url = URL(string: current_url)
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                self.endTask()
                self.showSimpleAlert(title: "Error", message: error!.localizedDescription)
            }
            else {
                do {
                    let json_response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                    
                    DispatchQueue.main.async {
                        self.onResponse(json_response: json_response)
                    }
                }
                catch {
                    self.endTask()
                    print("Error while fetching from \(self.current_url)")
                }
            }
        }
        
        task.resume()
    }
    
    func onResponse(json_response: Dictionary<String, AnyObject>){
        let results = json_response["results"] as! [Dictionary<String, AnyObject>]
        for result in results{
            
            if let poster = result["poster_path"] as? String{
                let movie = Movie()
                movie.id = result["id"]! as! Int
                movie.title = result["title"]! as! String
                movie.rating = result["vote_average"]! as! Double
                movie.release_date = result["release_date"]! as! String
                movie.overview = result["overview"]! as! String
                movie.thumbnail = images_base_url + poster
                movies.append(movie)
            }
        }
        
        table_view.reloadData()
        self.endTask()
    }
    
    func endTask(){
        loader.stopAnimating()
        in_call = false
    }

    func safelyLikeMovie(movie: Movie){
        if isMovieLiked(movie_id: movie.id){
            // Unlike
            let context = app_delegate.persistentContainer.viewContext
            let fetch_request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieEntry")
            fetch_request.returnsObjectsAsFaults = false
            
            do {
                let results = try context.fetch(fetch_request)
                for result in results as! [NSManagedObject] {
                    let id = result.value(forKey: "id") as! Int
                    if id == movie.id{
                        context.delete(result)
                        try context.save()
                    }
                }
            }
            catch {
                print("Error while clearing favorites")
            }
            
            if strategy_index == 0{
                safelyFetchMovies(searching: false)
            }
            else{
                like_button.setImage(UIImage(named: "unlike"), for: .normal)
            }
        }
        else{
            if !in_call{
                if !app_delegate.isConnected(){
                    let network_alert = UIAlertController(
                        title: "Network Error",
                        message: "Please check your internet connection",
                        preferredStyle: .alert
                    )
                    
                    network_alert.addAction(UIAlertAction(title: "Retry", style: .default) {UIAlertAction in
                        self.safelyLikeMovie(movie: movie)
                    })
                    
                    network_alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) {UIAlertAction in
                    })
                    
                    self.present(network_alert, animated: true, completion: nil)
                }
                else{
                    // Download image
                    in_call = true
                    loader.startAnimating()
                    let url = URL(string: movie.thumbnail)
                    
                    ImageDownloader.default.downloadImage(with: url!, options: [], progressBlock: nil){
                        (image, error, url, data) in
                        
                        self.loader.stopAnimating()
                        self.in_call = false
                        movie.thumbnail_data = data!
                        
                        // Store movie locally
                        let context = self.app_delegate.persistentContainer.viewContext
                        let entry = NSEntityDescription.insertNewObject(forEntityName: "MovieEntry", into: context)
                        
                        entry.setValue(movie.id, forKey: "id")
                        entry.setValue(movie.title, forKey: "title")
                        entry.setValue(movie.rating, forKey: "rating")
                        entry.setValue(movie.overview, forKey: "overview")
                        entry.setValue(movie.release_date, forKey: "release_date")
                        entry.setValue(movie.thumbnail, forKey: "thumbnail")
                        entry.setValue(movie.thumbnail_data, forKey: "thumbnail_data")
                        
                        do {
                            try context.save()
                            
                            if self.strategy_index == 0{
                                self.safelyFetchMovies(searching: false)
                            }
                            else{
                                self.like_button.setImage(UIImage(named: "like"), for: .normal)
                            }
                        }
                        catch {
                            print("Error saving movie")
                        }
                    }
                }
            }
        }
    }

    func loadFavorites(){
        movies.removeAll()
        
        let context = app_delegate.persistentContainer.viewContext
        let fetch_request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieEntry")
        fetch_request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(fetch_request)
            
            for result in results as! [NSManagedObject] {
                let fav_movie = Movie()
                fav_movie.id = result.value(forKey: "id") as! Int
                fav_movie.title = result.value(forKey: "title") as! String
                fav_movie.rating = result.value(forKey: "rating") as! Double
                fav_movie.release_date = result.value(forKey: "release_date") as! String
                fav_movie.overview = result.value(forKey: "overview") as! String
                fav_movie.thumbnail = result.value(forKey: "thumbnail") as! String
                fav_movie.thumbnail_data = result.value(forKey: "thumbnail_data") as! Data
                
                movies.append(fav_movie)
            }
            
            table_view.reloadData()
            if movies.count == 0{
                no_favorites.alpha = 1
            }
        }
        catch {
            print("Error")
        }
    }
    
}
