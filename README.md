# MoviesDirectory

A movies app that relies on the MoviesDB API and lets users fetch movies and filter them based on popular, top rated or now playing. Users can add movies to favorites which saves movies locally and downloads their images such that they can be viewed later without needing internet connection. Users can also share movies and view trailers and reviews.

Demo Video : https://www.dropbox.com/s/mil8p8dpqidgxrp/3-MoviesDirectory.mp4?dl=0